from django.contrib import admin
from .models import Publisher, Developer, Genre, Platform, Game
#from import_export.admin import ImportExportModelAdmin


# Register your models here.
admin.site.register(Publisher)
admin.site.register(Developer)
admin.site.register(Genre)
admin.site.register(Platform)
admin.site.register(Game)

#class ViewAdmin(ImportExportModelAdmin):
#	pass