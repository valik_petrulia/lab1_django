from django.db import models
from django.urls import reverse
import datetime

year_choices = []
for r in range(1980, (datetime.datetime.now().year+1)):
    year_choices.append((r,r))
	

class Publisher(models.Model):
	publisher_name = models.CharField(max_length = 30, unique = True)
	publisher_address = models.CharField(max_length = 30)
	publisher_foundation_year = models.IntegerField(choices = year_choices)

	def __str__(self):
		return self.publisher_name


class Developer(models.Model):
	developer_name = models.CharField(max_length = 30, unique = True)
	developer_employees = models.PositiveIntegerField()
	developer_foundation_year = models.IntegerField(choices = year_choices)
	developer_publisher = models.ForeignKey(Publisher, on_delete = models.CASCADE)

	def __str__(self):
		return self.developer_name


class Genre(models.Model):
	genre_name = models.CharField(max_length = 30, unique = True)
	
	def __str__(self):
		return self.genre_name


class Platform(models.Model):
	platform_name = models.CharField(max_length = 30, unique = True)

	def __str__(self):
		return self.platform_name
	

class Game(models.Model):
	game_release_year = models.IntegerField(choices = year_choices)
	game_name = models.CharField(max_length = 30)
	game_genre = models.ForeignKey(Genre, on_delete = models.CASCADE)
	game_developer = models.ForeignKey(Developer, on_delete = models.CASCADE)
	game_publisher = models.ForeignKey(Publisher, on_delete = models.CASCADE)
	game_platforms = models.ManyToManyField(Platform)
	#game_copies = models.PositiveIntegerField()

	def __str__(self):
		return self.game_name

	def get_absolute_url(self):
		return reverse('game-detail', kwargs = {'pk': self.pk})
	
