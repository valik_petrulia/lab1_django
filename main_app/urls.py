from django.urls import path
from .views import (
	GameListView, 
	GameDetailView,
	GameCreateView,
	GameDeleteView,
	PublisherListView,
	DeveloperListView,
    ChartView 
)	
from . import views

urlpatterns = [
    path('', GameListView.as_view(), name = 'main_app-home'),
    path('game/<int:pk>', GameDetailView.as_view(), name = 'game-detail'),
    path('game/create', GameCreateView.as_view(), name = 'game-create'),
    path('game/<int:pk>/delete', GameDeleteView.as_view(), name = 'game-delete'),
    path('about/', views.about, name = 'main_app-about'),
    path('publishers/', PublisherListView.as_view(), name = 'publishers-view'), 
    path('developers/', DeveloperListView.as_view(), name = 'developers-view'),
    path('diagrams/', ChartView.as_view(), name = 'diagrams-view'),
]