from django.shortcuts import render
from django.db.models import Sum
from django.views.generic import (
	ListView, 
	DetailView,
	CreateView,
	DeleteView,
	TemplateView
)
from .models import Publisher, Developer, Game, Genre, Platform


def home(request):
	context = {
		'games': Game.objects.all(),
		'publishers': Publisher.objects.all(),
		'developers': Developer.objects.all(),
		
	}
	return render(request, 'main_app/home.html', context)


class GameListView(ListView):
	model = Game
	template_name = 'main_app/home.html'
	context_object_name = 'games'


class GameDetailView(DetailView):
	model = Game


class GameCreateView(CreateView):
	model = Game
	fields = ['game_name', 'game_genre', 'game_publisher', 'game_developer', 'game_release_year', 'game_platforms']


class GameDeleteView(DeleteView):
	model = Game
	success_url = '/main_app'


class PublisherListView(ListView):
	model = Publisher
	template_name = 'main_app/publishers_view.html'
	context_object_name = 'publishers'


class DeveloperListView(ListView):
	model = Developer
	template_name = 'main_app/developers_view.html'
	context_object_name = 'developers'


class ChartView(TemplateView):
	template_name = 'main_app/chart.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context["emp"] = Developer.objects.all()
		context["copies"] = Game.objects.all()
		return context


def about(request):
	return render(request, 'main_app/about.html')

